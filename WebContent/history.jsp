<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>BoliPoint</title>
	<link rel='stylesheet' type='text/css' href='css/bootstrap.css' />
	<link rel='stylesheet' type='text/css' href='css/geral.css' />
	
</head>
<body>

			<nav class='navbar navbar-expand-lg navbar-light' style='background-color:#aaa'>
				<a class='navbar-brand' href='history.jsp'><img class='img-fluid' src='imagens/LogoBoliOf.svg'  style='width:60px;height:60px;'/>História</a>
					<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#a' aria-expanded='true' aria-controls='a' aria-label='Alterna'>
						<span class='navbar-toggler-icon'></span>
					</button>
				
				<div class='nav collapse navbar-collapse' id='a'>
					<ul class='navbar-nav mr-auto'>
						<li class='nav-item'>
							<a class='nav-link' href='Main.jsp'>Home</a>
						</li>
					
						
					</ul>
				</div>
			</nav>
			
		<h1>História do Boliche</h1>
		<div class='container-fluid cont'>
			<img class='img-fluid' src='imagens/bol4.jpg'/>
		</div>
		<div class='container-fluid'>
			<p class='fonte-small'>
				O boliche surgiu por volta no século III na Alemanha, mas há indícios de um esporte parecido praticado pelos egípcios 7 mil anos atrás ou pelos índios da Polinésia nos séculos seguintes. Na Inglaterra, um outro jogo semelhante também era praticano. O esporte era praticado na grama e tinha por objetivo colocar a bola o mais perto possível do alvo, sem derrubá-lo.

Mas foi na Alemanha que o boliche começou a encontrar a sua versão moderna.</p>

<p class='fonte-small'> O esporte nasceu como uma prática religiosa, na qual os fiéis atiraram pedras em seu “Kegel” (uma espécie de bastão que carregavam para a proteção). O “Kegel” representava o Céu. Assim, quem conseguisse derrubá-lo, poderia se considerar livre de pecado.

Existem várias referências ao esporte em toda a Idade Média na Alemanha. </p>

<p class='fonte-small'>

Em 1325, foram promulgadas leis que limitavam o que poderia ser apostado em partidas de boliche. Em 1463, um festival em Frankfurt tinha como principal atração justamente uma competição de boliche.

Assim, a modalidade se espalhou pelos outros países europeus. Por volta de 1650, os holandeses desenvolveram as primeiras regras do esporte. </p> 
<div class='container-fluid cont'>
	<img class='img-fluid ' src='imagens/bol3.jpg'>
</div>
<p class='fonte-small'> O boliche era disputado com nove pinos organizados em formato de diamante na pista. Esta versão continua sendo disputada, principalmente na Europa. Já a versão mais conhecida, com 10 pinos, que será jogada no Pan-Americano, foi criada nos Estados Unidos no século 19.

As regras atuais foram criadas em 1875, com o surgimento da Associação Nacional de Boliche dos Estados Unidos. A entidade não durou muito tempo, assim como a sua sucessora, a União Americana de Boliche Amador. Ambas, entretanto, ajudaram a consolidar o esporte nos Estados Unidos e a “exportar” a modalidade de 10 pinos para a Europa.

Em 1926, surgiu a Associação Internacional de Boliche, com a presença de Dinamarca, Finlândia, Alemanha, Holanda, Noruega e Estados Unidos. Já a Federação Internacional de Boliche, que hoje comanda o esporte, foi criada apenas em 1952.
			</p>
		</div>
 <script src='/javascripts/application.js' type='text/javascript' charset='utf-8' ></script>
	<script type="text/javascript" src='js/jquery.js'></script>
		<script type="text/javascript" src='js/bootstrap.js'></script>
	
		
</body>
</html>