<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>BoliPoint</title>
	<link rel='stylesheet' type='text/css' href='css/bootstrap.css' />
	<link rel='stylesheet' type='text/css' href='css/geral.css' />
	
</head>
<body>

			<nav class='navbar navbar-expand-lg navbar-light' style='background-color:#aaa'>
				<a class='navbar-brand' href='index.jsp'><img class='img-fluid' src='imagens/LogoBoliOf.svg'  style='width:60px;height:60px;'/>Home</a>
					<button class='navbar-toggler' type='button' data-toggle='collapse' data-target='#a' aria-expanded='true' aria-controls='a' aria-label='Alterna'>
						<span class='navbar-toggler-icon'></span>
					</button>
				
				<div class='nav collapse navbar-collapse' id='a'>
					<ul class='navbar-nav mr-auto'>
						
						<li class='nav-item'>
							<a class='nav-link' href='Cadastro.jsp'>Cadastro</a>
						</li>
						<li class='nav-item'>
							<a class='nav-link' href='Login.jsp'>Login</a>
						</li>
											
											
					</ul>
				</div>
			</nav>
	<div class='container-fluid cont '>
		<img class='img-fluid' alt="Imagem não carregada" src="imagens/BoliPoint1.svg">
	</div>
	
	<div class='container-fluid' >	
		<p class='fonte-small'>Seja bem vindo ao BoliPoint,
		 o melhor sistema para administrar os pontos de um jogo de boliche.
		  Se cadastre no link abaixo, e administre seu jogo de forma mais eficiente.
		  <a href='#'>Cadastre-se</a></p>
	</div>
	<div class='container-fluid cont'>
		<img class='img-fluid' src='imagens/bol2.jpg'/>
	</div>
	
	<div class='container-fluid'>
		<p class='fonte-small'>Com o boliPoint, você poderá gerenciar grandes campeonatos, 
		sem mais a demora pra anotar a pontuação e saber quem venceu, 
		precisará apenas colocar o tipo de pontuação, 
		e logo se revelará o vencedor.</p>                                                                                                                                                                    
	</div>
	
	<div class='container-fluid cont'>
		<img class='img-fluid' src='imagens/bol1.jpg' />
	</div>
	<div class='container-fluid'>
		<p class='fonte-small'>Conheça também a história do boliche no link logo em seguida.<a href='history.jsp'>História</a></p>
		
	</div>
	<footer class='footer'>
		<div class='container-fluid'>
			<div class='row'>
				<p style='font-size:3vmin;' >
					© 2020 Copyright BoliPoint web site.
				</p>
			</div>
		</div>
	</footer>
	
	
 <script src='/javascripts/application.js' type='text/javascript' charset='utf-8' ></script>
	<script type="text/javascript" src='js/jquery.js'></script>
		<script type="text/javascript" src='js/bootstrap.js'></script>
	
			
</body>

</html>