<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<c:import url="Frags/header.jsp" ></c:import>					
<link rel="stylesheet" type="text/css" href="css/main.css"/>
			<div><h3>Seja bem vindo. avaliador:</h3></div>
			
			<div>
				<button class='CrPart' data-element='#crPart'>Criar partida r�pida</button>
				<button class='CrCamp' data-element='#crCamp'>Criar campeonato</button>
				
			</div>
			<div class='container' id='crPart' style='display:none;'>
				<form method='post' action='inscricao_jogador.jsp'>
					<h4>Cria��o de partida</h4>
					<div class='form-group'>
						<label>Nome da partida</label>
						<input type='text' id='partName' name='partName' placeholder='Digite aqui o nome da partida' required/>
						
						<label>Tipo de partida</label>
						<select>
							<option value='Amistoso'>Amistoso</option>
							<option value='Treino'>Treino</option>
							<option value='Oficial'>Oficial</option>
						</select>
						<div class='form-group'>
							<label>Data da partida</label>
							<input type='date' name='data' id='data'/>
						</div>
						<div class='form-group'>
							<label></label>
						</div>
						<div class='form-group'>
							<input type='submit' value='Inscrever jogadores'/>	
						</div>
					</div>
				
				</form>
			</div>
				<div class='container' id='crCamp' style='display:none;'>
					<form method='post' action=''>
						<h4>Cria��o de campeonato</h4>
							<div class='form-group'>
								<label>Nome do campeonato</label>
								<input type='text' id='CampName' name='campName' placeholder='Digite o nome do campeonato' required/>
							</div>
							<div class='form-group'>
								<label>Tipo de campeonato</label>
								<select>
									<option value='Estadual'>Estadual</option>
									<option value='Nacional'>Nacional</option>
									<option value='Internacional'>Internacional</option>
								</select>
							</div>
							<div class='form-group'>
									<label>Escolha o estado onde ocorrer� o campeonato</label>
									<select required>
										<option value='Acre(AC)'>Acre(AC)</option>
										<option value='Alagoas(AL)'>Alagoas(AL)</option>
										<option value='Amap�(AP)'>Amap�(AP)</option>
										<option value='Amazonas(AM)'>Amazonas(AM</option>
										<option value='Bahia(BA)'>Bahia(BA</option>
										<option value='Cear�(CE)'>Cear�(CE)</option>
										<option value='Distrito Federal(DF)'>Distrito Federal(DF)</option>
										<option value='Esp�rito Santo(ES)'>Esp�rito Santo(ES)</option>
										<option value='Go�as(GO)'>Go�as(GO)</option>
										<option value='Maranh�o(MA)'>Maranh�o(MA)</option>
										<option value='Mato Grosso(MT)'>Mato Grosso(MT)</option>
										<option value='Mato Grosso do Sul(MS)'>Mato Grosso do Sul(MS)</option>
										<option value='Minas Gerais(MG)'>Minas Gerais(MG)</option>
										<option value='Par�(PA)'>Par�(PA)</option>
										<option value='Para�ba(PB)'>Para�ba(PB</option>
										<option value='Paran�(PR)'>Paran�(PR)</option>
										<option value='Pernambuco(PE)'>Pernambuco(PE)</option>
										<option value='Piau�(PI)'>Piau�(PI)</option>
										<option value='Rio de Janeiro(RJ)'>Rio de Janeiro(RJ)</option>
										<option value='Rio Grande do Norte(RN)'>Rio Grande do Norte(RN)</option>
										<option value='Rio Grande do Sul(RS)'>Rio Grande do Sul(RS)</option>
										<option value='Rond�nia(RO)'>Rond�nia(RO)</option>
										<option value='Roraima(RR)'>Roraima(RR)</option>
										<option value='Santa Catarina(SC)'>Santa Catarina(SC)</option>
										<option value='S�o Paulo(SP)'>S�o Paulo(SP)</option>
										<option value='Sergipe(SE)'>Sergipe(SE)</option>
										<option value='Tocatins(TO)'>Tocatins(TO)</option>
		
		
									</select>	
						</div>
						
					<div class='form-group'>
						<label>Digite o pa�s que ocorrer� o campeonato</label>
						<input type='text' name='paCam' id='paCam' required/>
					</div>
					<div class='form-group'>
						<label>Organizador</label>
						<input type='text' name='org' id='org' required/>
					</div>
					
					<div class='form-group'>
						<input type='submit' value='Inscrever jogadores'>
					</div>
			</form>				
		</div>
<c:import url="Frags/footer.jsp"></c:import>