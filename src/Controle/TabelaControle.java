package Controle;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.SQLException;
import Modelo.Tabela;
public class TabelaControle {
	public boolean insert(Tabela user) {
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getConexao().prepareStatement("INSERT INTO tabela(Um,dois,tres) VALUES(?,?,?);");
			ps.setString(1,user.getUm());
			ps.setString(2,user.getDois());
			ps.setString(3,user.getTres());
			if(!ps.execute()) {
				retorno = true;
			}
			con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de sql ao inserir Usu�rio"+e.getMessage());
			return false;
		}catch(Exception e){
			System.out.println("Erro geral ao inserir Usu�rio"+e.getMessage());
			return false;
		}
		return retorno;
	}
	public ArrayList<Tabela> SelecionarTodos(){
		try {
			ArrayList<Tabela> lista = null;
			Conexao conexao =  new Conexao();
			String sql = "SELECT * FROM tabela;";
			PreparedStatement ps = conexao.getConexao().prepareStatement(sql);
			if(ps.execute()) {
				lista = new ArrayList<Tabela>();
					ResultSet rs = ps.executeQuery();
					while(rs.next()) {
						Tabela user = new Tabela();
						user.setSett(rs.getInt("sett"));
						user.setUm(rs.getString("Um"));
						user.setDois(rs.getString("dois"));
						user.setTres(rs.getString("tres"));
						lista.add(user);
						
					}
					conexao.fecharConexao();
					return lista;
				
			}else {
				return lista;
			}
		}catch(SQLException e) {
			System.out.println("Erro de sql na Controle:"+e.getMessage());
			return null;
		}catch(Exception e) {
			System.out.println("Erro geral:"+e.getMessage());
			return null;
		}
	}
	public boolean UpdateTabela(Tabela user) {
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			String sql = "UPDATE Tabela SET Um = ?,dois = ?, tres = ? WHERE sett = ?;";
			PreparedStatement ps = con.getConexao().prepareStatement(sql);
			ps.setString(1,user.getUm());
			ps.setString(2,user.getDois());
			ps.setString(3,user.getTres());
			ps.setInt(4,user.getSett());
			if(!ps.execute()) {
				retorno = true;
			}
		}catch(SQLException e) {
			System.out.println("Erro de sql no updateDoUsuario:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral no Upadate:"+e.getMessage());
		}
		return retorno;
	}
	
	public boolean TruncateTable(Tabela user){
		try {
				Conexao con = new Conexao();
				String sql = "TRUNCATE TABLE tabela; ";
				PreparedStatement ps = con.getConexao().prepareStatement(sql);
				if(!ps.execute()) {
					return true;
				}else {
					return false;
				}
		}catch(SQLException e) {
			System.out.println("Erro de sql no updateDoUsuario:"+e.getMessage());
		}catch(Exception e) {
			System.out.println("Erro geral no Upadate:"+e.getMessage());
		}
		return false;
		
	}
}
