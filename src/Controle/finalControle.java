package Controle;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import Modelo.Final;
public class finalControle {
	public boolean insert(Final user) {
		boolean retorno = false;
		try {
			Conexao con = new Conexao();
			PreparedStatement ps = con.getConexao().prepareStatement("INSERT INTO Pfinal(id,ponto1,ponto2,dia,mes,ano) VALUES(?,?,?,?,?,?);");
			ps.setInt(1,user.getId());
			ps.setString(2,user.getPri());
			ps.setString(3,user.getSec());
			ps.setString(4,user.getDia());
			ps.setString(5,user.getMes());
			ps.setString(6,user.getAno());
			if(!ps.execute()) {
				retorno = true;
			}
			con.fecharConexao();
		}catch(SQLException e) {
			System.out.println("Erro de sql ao inserir Usuario"+e.getMessage());
			return false;
		}catch(Exception e){
			System.out.println("Erro geral ao inserir Usu�rio"+e.getMessage());
			return false;
		}
		return retorno;
	}
}
