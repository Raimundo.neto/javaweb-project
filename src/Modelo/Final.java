package Modelo;

public class Final {
	
		private int id;
		private String Pri;
		private String Sec;
		private String dia;
		private String mes;
		private String ano;
		
		public Final() {
			this.id = 0;
			this.Pri = "";
			this.Sec = "";
			this.dia = "";
			this.mes = "";
			this.ano = "";
		}
		public int getId() {
			return id;
		}
		public void setId(int id) {
			this.id = id;
		}
		public String getPri() {
			return Pri ;
		}
		public void setPri(String pri) {
			this.Pri = pri;
		}
		public String getSec() {
			return Sec;
		}
		public void setSec(String sec) {
			this.Sec = sec;
		}
		public String getDia() {
			return dia;
		}
		public void setDia(String dia) {
			this.dia = dia;
		}
		public String getMes() {
			return mes;
		}
		public void setMes(String mes) {
			this.mes = mes;
		}
		public String getAno() {
			return ano;
		}
		public void setAno(String ano) {
			this.ano = ano;
		}
}
