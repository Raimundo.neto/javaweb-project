package Servlets;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import Modelo.Usuario;
import Controle.UsuarioControle;

/**
 * Servlet implementation class HomeController
 */
@WebServlet("/HomeController")
public class HomeController extends HttpServlet {
	private static final long serialVersionUID = 1L;
 
    public HomeController() {
        super();
        // TODO Auto-generated constructor stub
    }
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String acao = request.getParameter("form").toString();
    	switch(acao) {
    		case "buscaId":
    				Usuario usuario = new UsuarioControle().selecionarUm(Integer.parseInt(request.getParameter("buscaId")));
    				if(usuario != null) {
    					request.setAttribute("usuario",usuario);
    					request.getRequestDispatcher("UpdateUsuario.jsp").forward(request, response);
    				}
    			break;
    		case "buscaDel":
				Usuario del = new UsuarioControle().selecionarUm(Integer.parseInt(request.getParameter("buscaId")));
				if(del != null) {
					request.setAttribute("usuario",del);
					request.getRequestDispatcher("deletar.jsp").forward(request, response);
				}
				break;
    		case "adicionar":
    			Usuario user = new Usuario();
    			user.setUser(request.getParameter("user"));
    			user.setEmail(request.getParameter("email"));
    			user.setSenha(request.getParameter("senha"));
    			UsuarioControle control = new UsuarioControle();
    			if(control.insert(user)) {
    				request.getRequestDispatcher("index.jsp").forward(request,response);
    			}else {
    				request.getRequestDispatcher("404.jsp").forward(request, response);
    			}
    		break;
    		
    		case "editar":
    				
    				if(request.getParameter("idUser") != null && request.getParameter("senha").equals(request.getParameter("conSenha"))) {
    					Usuario editar  = new Usuario();
    					editar.setId(Integer.parseInt(request.getParameter("numId")));
        				editar.setUser(request.getParameter("usuario"));
        				editar.setEmail(request.getParameter("email"));
        				editar.setSenha(request.getParameter("senha"));
        				if(new UsuarioControle().UpdateUser(editar)) {
        					request.getRequestDispatcher("index.jsp").forward(request, response);
        				}else{
        					request.getRequestDispatcher("404.jsp").forward(request, response);
        				}
    				}
    			
    		break;
    		case "del":
    				if(request.getParameter("removerId") != null) {
    					if(new UsuarioControle().delete(Integer.parseInt(request.getParameter("removerId")))) {
    						request.getRequestDispatcher("index.jsp").forward(request, response);
    					}
    				}
    			break;
    		default:
    			request.getRequestDispatcher("404.jsp").forward(request, response);
    	}
    	
    }
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String pagina = request.getParameter("pagina").toString();
		
		switch(pagina) {
			case "home":
				ArrayList<Usuario> listarUsuarios = new UsuarioControle().SelecionarTodos();
				request.setAttribute("lista", listarUsuarios);
				request.getRequestDispatcher("home.jsp").forward(request, response);
				break;
			case "adicionar":
				request.getRequestDispatcher("adicionar.jsp").forward(request, response);
				break;
			case "editar":
				if(request.getParameter("usuario") != null) {
					Usuario usuarioEdit = new UsuarioControle().selecionarUm(Integer.parseInt(request.getParameter("usuario")));
					request.setAttribute("usuario",usuarioEdit);
				}
				request.getRequestDispatcher("UpdateUsuario.jsp").forward(request, response);
				break;
			case "deletar":
				if(request.getParameter("usuario") != null) {
					Usuario usuarioDel= new UsuarioControle().selecionarUm(Integer.parseInt(request.getParameter("usuario")));
					request.setAttribute("usuario",usuarioDel);
				}
				request.getRequestDispatcher("deletar.jsp").forward(request, response);
				break;
			default:
				request.getRequestDispatcher("404.jsp").forward(request, response);
				
		}
	}

}
